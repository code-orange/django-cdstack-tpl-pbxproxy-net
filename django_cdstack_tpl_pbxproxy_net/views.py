import copy
import ipaddress

from django.template import engines

from django_cdstack_deploy.django_cdstack_deploy.func import generate_config_static
from django_cdstack_tpl_cloudrouter_v3.django_cdstack_tpl_cloudrouter_v3.views import (
    handle as handle_cloudrouter_v3,
)
from django_cdstack_tpl_deb.django_cdstack_tpl_deb.views import handle as handle_deb
from django_cdstack_tpl_deb.django_cdstack_tpl_deb.views import get_os_release
from django_cdstack_tpl_deb_stretch.django_cdstack_tpl_deb_stretch.views import (
    get_apt_packages,
    get_apt_sources,
    get_apt_prefs,
    clean_new_lines,
    zip_add_file,
)


def handle(zipfile_handler, template_opts, cmdb_host, skip_handle_os=False):
    django_engine = engines["django"]

    module_prefix = "django_cdstack_tpl_pbxproxy_net/django_cdstack_tpl_pbxproxy_net"

    if "apt_packages" not in template_opts:
        template_opts["apt_packages"] = get_apt_packages(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "fail2ban_apps" not in template_opts:
        template_opts["fail2ban_apps"] = copy.deepcopy(template_opts["apt_packages"])

    if "apt_sources" not in template_opts:
        template_opts["apt_sources"] = get_apt_sources(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "apt_preferences" not in template_opts:
        template_opts["apt_preferences"] = get_apt_prefs(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "os_release" not in template_opts:
        template_opts["os_release"] = get_os_release(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    pbx_network_dynamic = dict()

    pbx_network_ids = list()

    openvpn_dnat = dict()

    for key in template_opts.keys():
        if key.startswith("pbx_network") and key.endswith("_vlan_id"):
            key_ident = key[:-8]

            network_namespace_opts = dict()

            pbx_network_split = key.split("_")
            pbx_network_id = int(pbx_network_split[2])
            pbx_network_eth_port = template_opts[key_ident + "_eth_port"]
            network_namespace_opts["pbx_network_id"] = pbx_network_id
            network_namespace_opts["pbx_network_eth_port"] = pbx_network_eth_port

            pbx_network_vlan_id = template_opts[key_ident + "_vlan_id"]
            network_namespace_opts["pbx_network_vlan_id"] = pbx_network_vlan_id

            pbx_network_ids.append(pbx_network_id)

            # CALC PBX NAT IP
            pbx_nat_start_ip_int = int(ipaddress.IPv4Address("100.65.1.0"))
            pbx_nat_ip_int = pbx_nat_start_ip_int + pbx_network_id
            pbx_nat_ip = str(ipaddress.IPv4Address(pbx_nat_ip_int))
            network_namespace_opts["pbx_network_ns_ip"] = pbx_nat_ip

            # VLAN ACCESS
            pbx_network_dynamic[f"network_iface_{pbx_network_eth_port}_fw_zone"] = (
                "pbxnat"
            )
            pbx_network_dynamic[f"network_iface_ns{pbx_network_id}_fw_zone"] = "pbxnat"

            # NAMESPACE ACCESS
            pbx_network_dynamic[f"network_iface_ns{pbx_network_id}_hwaddr"] = (
                template_opts["network_iface_" + pbx_network_eth_port + "_hwaddr"]
            )
            pbx_network_dynamic[f"network_iface_ns{pbx_network_id}_mode"] = "manual"
            pbx_network_dynamic[
                f"network_iface_ns{pbx_network_id}_configure_interfaces"
            ] = "yes"
            pbx_network_dynamic[f"network_iface_ns{pbx_network_id}_peer_netns"] = (
                f"ns{pbx_network_id}"
            )
            pbx_network_dynamic[f"network_iface_ns{pbx_network_id}_veth_mode"] = "l2"

            pbx_network_dynamic[f"network_iface_ns{pbx_network_id}_post_up"] = str()
            pbx_network_dynamic[f"network_iface_ns{pbx_network_id}_post_up"] += (
                f"ip link set {pbx_network_eth_port} netns ns{pbx_network_id}" + "\r\n"
            )
            pbx_network_dynamic[f"network_iface_ns{pbx_network_id}_post_up"] += (
                f"brctl addif brpbxnat ns{pbx_network_id}" + "\r\n"
            )

            # TODO: replace NAT with firewall
            pbx_network_dynamic[f"network_iface_ns{pbx_network_id}_post_up"] += (
                f"ip netns exec ns{pbx_network_id} iptables -t mangle -A FORWARD -p tcp -m tcp --tcp-flags SYN,RST SYN -m tcpmss --mss 1281:9000 -j TCPMSS --set-mss 1280"
                + "\r\n"
            )
            pbx_network_dynamic[f"network_iface_ns{pbx_network_id}_post_up"] += (
                f"ip netns exec ns{pbx_network_id} iptables -t nat -A POSTROUTING -o ns{pbx_network_id} -j MASQUERADE"
                + "\r\n"
            )
            pbx_network_dynamic[f"network_iface_ns{pbx_network_id}_post_up"] += (
                f"ip netns exec ns{pbx_network_id} iptables -t nat -A PREROUTING -i ns{pbx_network_id} -d {pbx_nat_ip} -p udp -m udp --dport 4444 -j RETURN"
                + "\r\n"
            )
            pbx_network_dynamic[f"network_iface_ns{pbx_network_id}_post_up"] += (
                f"ip netns exec ns{pbx_network_id} iptables -t nat -A PREROUTING -i ns{pbx_network_id} -d {pbx_nat_ip} -j DNAT --to-destination 100.64.0.2"
                + "\r\n"
            )

            # OPENVPN
            network_namespace_opts["openvpn_namespace_ca"] = template_opts[
                key_ident + "_openvpn_namespace_ca"
            ]
            network_namespace_opts["openvpn_namespace_cert"] = template_opts[
                key_ident + "_openvpn_namespace_cert"
            ]
            network_namespace_opts["openvpn_namespace_key"] = template_opts[
                key_ident + "_openvpn_namespace_key"
            ]
            network_namespace_opts["openvpn_namespace_dh"] = template_opts[
                key_ident + "_openvpn_namespace_dh"
            ]

            config_template_file = open(
                module_prefix
                + "/templates/config-fs/dynamic/etc/network/interfaces.ns",
                "r",
            ).read()
            config_template = django_engine.from_string(config_template_file)

            zip_add_file(
                zipfile_handler,
                f"etc/network/interfaces.ns{pbx_network_id}",
                clean_new_lines(config_template.render(network_namespace_opts)),
            )

            remote_routes = list()

            for key in template_opts.keys():
                if key.startswith(key_ident + "_openvpn_client_") and key.endswith(
                    "_name"
                ):
                    key_client_ident = key[:-5]

                    openvpn_client_name = template_opts[key]

                    for key in template_opts.keys():
                        if key.startswith(
                            key_client_ident + "_remote_route"
                        ) and key.endswith("_network"):
                            key_client_route_route_ident = key[:-8]

                            remote_route = dict()

                            remote_route["network"] = template_opts[
                                key_client_route_route_ident + "_network"
                            ]
                            remote_route["netmask"] = template_opts[
                                key_client_route_route_ident + "_netmask"
                            ]

                            remote_routes.append(remote_route)

                    config_template_file = open(
                        module_prefix
                        + "/templates/config-fs/dynamic/etc/openvpn/pbx-server/openvpn_namespace/client-config",
                        "r",
                    ).read()
                    config_template = django_engine.from_string(config_template_file)

                    zip_add_file(
                        zipfile_handler,
                        f"etc/openvpn/pbx-server/ns{pbx_network_id}/ccd/"
                        + openvpn_client_name,
                        config_template.render({"remote_routes": remote_routes}),
                    )

                    network_namespace_opts["remote_routes"] = remote_routes

            config_template_file = open(
                module_prefix
                + "/templates/config-fs/dynamic/etc/openvpn/pbx-server/openvpn_namespace.conf",
                "r",
            ).read()
            config_template = django_engine.from_string(config_template_file)

            zip_add_file(
                zipfile_handler,
                f"etc/openvpn/pbx-server/ns{pbx_network_id}.conf",
                config_template.render(network_namespace_opts),
            )

            # OPENVPN DNAT
            openvpn_dnat[f"firewall_rule_dnat_{pbx_network_id}_source_zone"] = "net"
            openvpn_dnat[f"firewall_rule_dnat_{pbx_network_id}_dest_host"] = pbx_nat_ip
            openvpn_dnat[f"firewall_rule_dnat_{pbx_network_id}_dest_port"] = "4444"
            openvpn_dnat[f"firewall_rule_dnat_{pbx_network_id}_dest_zone"] = "pbxnat"
            openvpn_dnat[f"firewall_rule_dnat_{pbx_network_id}_local_port"] = str(
                10000 + pbx_network_id
            )
            openvpn_dnat[f"firewall_rule_dnat_{pbx_network_id}_proto"] = "udp"

    template_opts = {**template_opts, **pbx_network_dynamic, **openvpn_dnat}

    template_opts["pbx_network_ids"] = pbx_network_ids

    generate_config_static(zipfile_handler, template_opts, module_prefix)
    handle_cloudrouter_v3(zipfile_handler, template_opts, cmdb_host, skip_handle_os)

    return True
